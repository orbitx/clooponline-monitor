#include <MonitoringManager.h>
#include <thread>

#define WRITE_MAP(CLOOP_TYPE, TYPE) \
   mSharedMemoryHub->writeMapEntry(CLOOP_TYPE, mMonitoringParentName, ins.first, \
                                   *(boost::any_cast<TYPE*>(ins.second)));

using namespace std;

MonitoringManager::MonitoringManager(int monitoringTimeStep, string monitoringParentName, string hostip, int port)
{
   mMonitoringTimeStep = monitoringTimeStep;
   mMonitoringParentName = monitoringParentName;
   mSharedMemoryHub = clooponline::DataHub::connectToRedis(hostip, port);
}

void MonitoringManager::monitoringIterated()
{
   for (auto& n : mIterationNotifyee)
   {
      n();
   }
}

void MonitoringManager::addIterationNotifyee(std::function<void()> notifyee)
{
   mIterationNotifyee.push_back(notifyee);
}

void MonitoringManager::run()
{
   mShutdownFlag = false;
   mLoopThread = thread(&MonitoringManager::loop, this);
}

void MonitoringManager::stop()
{
   mShutdownFlag = true;
   mLoopThread.join();
}

void MonitoringManager::loop()
{
   while(!mShutdownFlag)
   {
      monitoringIterated();
      this_thread::sleep_for(chrono::seconds(mMonitoringTimeStep));
      flushMonitoring();
   }
}

void MonitoringManager::flushMonitoring()
{
   if (!mSharedMemoryHub)
      return;

   for (auto& ins : mInstances)
   {
      if (ins.second.type() == typeid(bool*))
      {
         WRITE_MAP(clooponline::Boolean, bool)
      }
      else if (ins.second.type() == typeid(int*))
      {
         WRITE_MAP(clooponline::Integer, int);
      }
      else if (ins.second.type() == typeid(uint*))
      {
         //TODO: update Datahub to accept unsigned values as well!
         mSharedMemoryHub->writeMapEntry(clooponline::Integer, mMonitoringParentName, ins.first,
                                   int(*(boost::any_cast<uint*>(ins.second))));
      }
      else if (ins.second.type() == typeid(long*))
      {
         WRITE_MAP(clooponline::Long, long);
      }
      else if (ins.second.type() == typeid(ulong*))
      {
         mSharedMemoryHub->writeMapEntry(clooponline::Long, mMonitoringParentName, ins.first,
                                         long(*(boost::any_cast<ulong*>(ins.second))));
      }
      else if (ins.second.type() == typeid(double*))
      {
         WRITE_MAP(clooponline::Double, double);
      }
      else if (ins.second.type() == typeid(std::string*))
      {
         WRITE_MAP(clooponline::String, std::string);
      }
   }

   mSharedMemoryHub->writeMapEntry(clooponline::Integer, mMonitoringParentName, "lastupdate", int(time(0)));
}