#include "Logger.h"

using namespace std;

Logger::Logger(string fileNamePrefix, LogSeverity minFilter)
{
   mMinFilter = minFilter;
   mShutdownFlag = false;

   if (fileNamePrefix != "")
   {
      logging::add_file_log
         (
            keywords::file_name = fileNamePrefix + "_%Y-%m-%d_%H-%M-%S.%N.log",
            keywords::rotation_size = 10 * 1024 * 1024,
            keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
            keywords::auto_flush = true,
            keywords::format =
               (
                  expr::stream << expr::attr <unsigned int> ("LineID") << " |"
                       << " [" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S")
                       << "]"
                       << " <" << logging::trivial::severity << "> "
                       << expr::smessage
               )
         );
   }
   
  logging::core::get()->set_filter
     (
        logging::trivial::severity >= (logging::trivial::severity_level)mMinFilter
     );

   logging::add_console_log(std::cout, boost::log::keywords::format =
            (
               expr::stream <<  "[" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S") << "]"
                            << " <" << logging::trivial::severity << "> : "
                            << expr::smessage
            )
   );

   logging::add_common_attributes();
}

bool Logger::lockBuffer(LogSeverity forSeverity)
{
   if (forSeverity >= mMinFilter)
   {
      mBufferLock.lock();
      mCurrentSevirity = forSeverity;
      return true;
   }
   else
      return false;
}

void Logger::flushLogBuffer()
{
   unique_lock<mutex> ulk(mQueueLock);
   mLoggingQueue.push(mLogBuffer.str());
   mLogBuffer.str("");
   mBufferLock.unlock();
   ulk.unlock();
   mLogCV.notify_one();
}

void Logger::stop()
{
   unique_lock<mutex> ulk(mQueueLock);
   mShutdownFlag = true;
   mBufferLock.unlock();
   ulk.unlock();
   mLogCV.notify_one();
   mLogThread.join();
}

void Logger::run()
{
   mShutdownFlag = false;
   mLogThread = std::thread(&Logger::processLogs, this);
}

void Logger::processLogs()
{
   while (!mShutdownFlag)
   {
      unique_lock<mutex> ulk(mQueueLock);

      if (mLoggingQueue.empty())
      {
         mLogCV.wait(ulk);
      }

      while (!mLoggingQueue.empty())
      {
         BOOST_LOG_SEV(mLocalLogger, (logging::trivial::severity_level) mCurrentSevirity) << mLoggingQueue.front();
         mLoggingQueue.pop();
      }

      ulk.unlock();
   }
}

Logger& operator<<(Logger& logger, PacketParser* ppPtr)
{
   logger.mLogBuffer << *ppPtr;
   return logger;
}