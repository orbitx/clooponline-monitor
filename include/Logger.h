#pragma once

#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>

namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

enum LogSeverity
{
   LOG_SEVERITY_TRACE = logging::trivial::trace,
   LOG_SEVERITY_DEBUG,
   LOG_SEVERITY_INFO,
   LOG_SEVERITY_WARNING,
   LOG_SEVERITY_ERROR,
   LOG_SEVERITY_FATAL,
   LOG_SEVERITY_NO_LOG
};

template<class T> class Singleton;
class PacketParser;
std::ostream& operator<<(std::ostream& out, const PacketParser& obj);

class Logger
{
   friend class Singleton<Logger>;
   template <class T>
   friend Logger& operator <<(Logger&, T);
   template <class T>
   friend Logger& operator <<(Logger&, std::shared_ptr<T>);
   friend Logger& operator <<(Logger&, PacketParser*);

public:
   bool lockBuffer(LogSeverity forSeverity);
   void flushLogBuffer();
   void run();
   void stop();
   
private:
   Logger(std::string fileNamePrefix = "", LogSeverity minFilter = LOG_SEVERITY_INFO);
   void processLogs();
   
   src::severity_logger<logging::trivial::severity_level> mLocalLogger;
   std::stringstream mLogBuffer;
   LogSeverity mMinFilter;
   LogSeverity mCurrentSevirity;
   std::mutex  mBufferLock;
   std::queue<std::string> mLoggingQueue;
   std::thread mLogThread;
   std::mutex  mQueueLock;
   std::condition_variable mLogCV;
   bool        mShutdownFlag;
};

template <class T>
Logger& operator<<(Logger& logger, T logMsg)
{
   logger.mLogBuffer << logMsg;
   return logger;
}

template <class T>
Logger& operator<<(Logger& logger, std::shared_ptr<T> logMsgPtr)
{
   logger.mLogBuffer << *logMsgPtr;
   return logger;
}

extern Logger& operator<<(Logger& logger, PacketParser* ppPtr);