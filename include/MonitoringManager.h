#pragma once

#include <map>
#include <string>
#include <boost/any.hpp>
#include <DataHub/DataHub.h>
#include <thread>

template<class T> class Singleton;

class MonitoringManager
{
   friend class Singleton<MonitoringManager>;

public:

   template <class T>
   void addMonitoringVariable(std::string monitorName, T* variable)
   {
      mInstances[monitorName] = variable;
   }

   void addIterationNotifyee(std::function<void()> notifyee);
   void run();
   void stop();

protected:
   MonitoringManager(int monitoringTimeStep, std::string monitoringParentName, std::string hostip, int port);
   MonitoringManager() { }

   void flushMonitoring();
   void monitoringIterated();
   void loop();

   std::map<std::string, boost::any>  mInstances;
   std::vector<std::function<void()>> mIterationNotifyee;
   int                                mMonitoringTimeStep;
   std::string                        mMonitoringParentName;
   clooponline::DataHub*              mSharedMemoryHub;
   std::thread                        mLoopThread;
   bool                               mShutdownFlag;
};