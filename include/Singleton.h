#pragma once

#include <mutex>
#include <memory>

template <class T>
class Singleton
{
public:
   template<class... ArgTypes>
   static std::shared_ptr<T> getInstance(ArgTypes... args)
   {
      if (mInstance)
         return mInstance;

      static std::mutex m;
      std::lock_guard<std::mutex> l(m);

      if (mInstance)
         return mInstance;
      else
         return construct(args...);
   }
   
   template<class... ArgTypes>
   static std::shared_ptr<T> construct(ArgTypes... args)
   {
      if (mInstance)
         return nullptr;

      mInstance.reset(new T(args...));
      return mInstance;
   }

protected:
   static std::shared_ptr<T> mInstance;
};

template <class T>
std::shared_ptr<T> Singleton<T>::mInstance = nullptr;

